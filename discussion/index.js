// Repetition Control Structures

/*
	3 Types of Looping Constructs
		1. While Loop
		2. Do-while Loop
		3. For Loop
*/

/*
	While loop
	- Syntax
		while(expression/condition) {
			statement/s;
		}
*/

let count = 5;

while(count !== 0) {
	console.log("While: " + count);
	count--; //count = count - 1, count -= 1
}

/*
Expected output:
	While 5
	While 4
	While 3
	While 2
	While 1
*/

let i = 1
while(i <= 5) {
	console.log("While: " + i);
	i++;
}


/*
Expected output:
	While 1
	While 2
	While 3
	While 4
	While 5
*/

/*
	let count = 0;

	while (count < 5) {
		++count;
		console.log(count);
	}

*/

/*
	Do While
		Syntax:
			do {
				statement;
			} while (expression/condition)
*/


let num1 = 1;

do {
	console.log("Do-while: " + num1);
	num1++;
} while (num1 < 6)

console.log("Number on prompt:");

// let num2 = Number(prompt("Give me a number"));

// do {
// 	console.log("Do While: " + num2);
// 	num2 += 1;
// } while(num2 < 10)

/*
	For Loop
	Syntax:
		for(initialization; expression/condition; finalExpression) {
			statement;
		}
*/

console.log("For Loop");


for (let num3 = 1; num3 <= 5; num3++) {
	console.log(num3);
}

console.log("For loop with prompt");

// let newNum = Number(prompt("Enter a number"));

// for (let num4 = newNum; num4 < 10; num4++) {
// 	console.log(num4);
// }

// Mini Activity

let input = Number(prompt("Input a number"))

for (let i = 1; i <= input; i++) {
	console.log(i);
}

console.log("String Iteration");

let myString = "Juan";

console.log(myString.length);

console.log(myString[0]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Displaying individual letters in a string")
// myString.length == 4
for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

let myName = "AloNzO";

for (let i = 0; i < myName.length; i++) {
	// if the character of the name is a vowel, instead of diplaying the character display number 3;
	if (
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'u' ||
		myName[i].toLowerCase() == 'u'
	) {
		console.log(3);
	}
	else {
		console.log(myName[i]);
	}
}

/*
	Continue and Break statements
		 - Continue statements allows the code to go to the next of iteration of the loop without finishing the execution of all statements in a code block
		 - Break statement is used to terminate the current loop once a match has been found.
*/


for(let count = 0; count <= 20; count ++) {
	if (count % 2 === 0) {
		continue;
	}

	console.log("Continue and Break: " + count);

	if (count > 10) {
		break;
	}
}